/**
 * Created by hardikranjan on 16/08/20.
 */

public without sharing class CustomListViewController {

    @AuraEnabled
    public static List<CustomListViewWrapper> getListValuesForSObject(String sObjectInfo) {

        List<CustomListViewWrapper> listViewWrappers = new List<CustomListViewWrapper>();

        for (ListView listView : [
                SELECT Id, Name, DeveloperName
                FROM ListView
                WHERE SObjectType = :sObjectInfo
                ORDER BY Name ASC
        ]) {
            CustomListViewWrapper customListViewWrapper = new CustomListViewWrapper();
            customListViewWrapper.label = listView.Name;
            customListViewWrapper.developerName = listView.DeveloperName;
            listViewWrappers.add(customListViewWrapper);
        }

        return listViewWrappers;
    }

    public class CustomListViewWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String developerName;
    }

}