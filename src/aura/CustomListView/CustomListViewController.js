/**
 * Created by hardikranjan on 16/08/20.
 */

({
    doInit : function(component, event, helper) {

		var action = component.get("c.getListValuesForSObject");
		action.setParams({
			"sObjectInfo" : component.get("v.objectInfo")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var listViewResult = response.getReturnValue();
				if(listViewResult.length > 0){
					component.set("v.listViewResult",listViewResult);
					component.set("v.currentListViewName", listViewResult[0].developerName);
					component.set("v.renderListView", true);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message while getting list views: " + errors[0].message);
					}
				}
			}
		});
		$A.enqueueAction(action);
	},

	onListViewChange: function(component, event, helper) {
	    console.log('Value: ' + event.getSource().get('v.value'));
		component.set("v.renderListView", false);
		let listViewName = event.getSource().get('v.value');
		component.set("v.currentListViewName", listViewName);
		component.set("v.renderListView", true);
	},
});